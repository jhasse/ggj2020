![Screenshot](screenshot.png)

# About the Game
Your controller is broken and you need to fix it, but so do the other players. Unlock new moves as the controller gets repaired. WHO CAN FIX THE CONTROLLER FIRST?

See also: https://globalgamejam.org/2020/games/controller-brawler-repair-force-7

# Making of
Read more about the game creation with videos: https://jhasse.gitlab.io/ggj2020

# Build
```
cmake -Bbuild -H. -DCMAKE_EXPORT_COMPILE_COMMANDS=1 -DCMAKE_BUILD_TYPE=Debug -GNinja
ninja -Cbuild
```
