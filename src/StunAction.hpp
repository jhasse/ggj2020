#pragma once

#include "GameObject.hpp"

#include <box2d/box2d.h>

class StunAction : public GameObject {

public:
	StunAction(b2World& world, jngl::Vec2 position, bool lethal);
    ~StunAction();
	virtual int getStunTime();
    bool step() override;
	void draw() const override;
	bool isLethal() const;

protected:
	virtual void CreateFixture(b2Shape& shape, uint16 bit_mask);
	int stun_time = 0;

private:
	bool lethal;
};
