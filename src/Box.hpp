#pragma once

#include "GameObject.hpp"

#include <box2d/box2d.h>
#include <jngl.hpp>

class Box : public GameObject {
public:
	Box(b2World&, jngl::Vec2 position);
	bool step() override;
	void draw() const override;

	void onContact(GameObject*) override;

private:
	jngl::Sprite sprite{"box"};
};
