#include "WinningScreen.hpp"

#include "Game.hpp"
#include "engine/SkeletonDrawable.hpp"
#include "Intro.hpp"

int WinningScreen::finished_count = 0;

void WinningScreen::animationStateListener(spAnimationState* state, spEventType type,
                                           spTrackEntry* entry, spEvent* event) {
	++finished_count;

	switch (type) {
	case SP_ANIMATION_INTERRUPT:
		break;

	default:
		break;
	}
}

WinningScreen::WinningScreen(std::shared_ptr<jngl::Work> game) : game(std::move(game)) {
	finished_count = 0;
	// Hier die Spine JSON laden und dann:
	//
	std::string skeletonFile = "intro/intro.json";
	spAtlas* atlas = spAtlas_createFromFile("intro/intro.atlas", 0);
	spSkeletonJson* json = spSkeletonJson_create(atlas);
	// json->scale = scale;

	skeletonData = spSkeletonJson_readSkeletonDataFile(json, (skeletonFile).c_str());
	if (!skeletonData) {
		// this->loading = RESOURCE_FAILED_LOADING;
		throw std::runtime_error("Could not parse JSON " + skeletonFile);
	}
	spSkeletonJson_dispose(json);

	spAnimationStateData* animationStateData = spAnimationStateData_create(skeletonData);

	skeleton = std::make_unique<spine::SkeletonDrawable>(skeletonData, animationStateData);
	spAnimationState_setAnimationByName(skeleton->state, 0, "winning", true);
	skeleton->state->listener = (spAnimationStateListener) & this->animationStateListener;

	skeleton->step();
}

WinningScreen::~WinningScreen() = default;

void WinningScreen::step() {
	++time;
	game->step();
	if (time < 90) {
		return;
	}
	if (dynamic_cast<const Game&>(*game).playersAlive() > 1) {
		// Der Spieler muss die anderen noch umbringen:
		jngl::setWork(std::move(game));
		return;
	}
	if (!skeleton || finished_count > 8) {
		jngl::setWork(std::make_shared<Intro>());
		return;
	}
	skeleton->step();
}

void WinningScreen::draw() const {
	game->draw();
	if (time < 300) {
		return;
	}
	jngl::pushMatrix();
	jngl::scale(0.55);
	jngl::translate(540, 400);
	skeleton->draw();
	jngl::popMatrix();
}
