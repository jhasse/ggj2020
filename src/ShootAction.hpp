#pragma once

#include "engine/Animation.hpp"
#include "StunAction.hpp"

#include <jngl/sprite.hpp>

class ShootAction : public StunAction {
public:
	ShootAction(b2World& world, uint16 bit_mask, jngl::Vec2 position, jngl::Vec2 direction, bool lethal);

	void draw() const override;
	void drawReflection() const override;
	bool step() override;
    void onContact(GameObject*) override;

private:
	float speed;
	jngl::Vec2 direction;
	bool destroy = false;
	Animation ball;
	jngl::Sprite shadow{"shadow"};
};
