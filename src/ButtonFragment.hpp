#pragma once

#include "ButtonType.hpp"
#include "GameObject.hpp"

#include <box2d/box2d.h>
#include <jngl.hpp>

class ButtonFragment : public GameObject, public std::enable_shared_from_this<ButtonFragment> {
public:
	/// \a partNr von 0 bis 3
	ButtonFragment(b2World&, ButtonType, int partNr = rand() % 4);
	~ButtonFragment();

	ButtonFragment(const ButtonFragment&) = delete;
	ButtonFragment& operator=(const ButtonFragment&) = delete;
	ButtonFragment(ButtonFragment&&) = delete;
	ButtonFragment& operator=(ButtonFragment&&) = delete;

	bool step() override;
	void draw() const override;
	void drawReflection() const override;

	void collect();

	jngl::Vec2 getPosition() const override;

	void setTargetPosition(jngl::Vec2);
	jngl::Vec2 getTargetPosition() const;

	ButtonType getType() const;
	bool isSamePart(const ButtonFragment& other);

	double getZIndex() const override;

private:
	/// Vector der angibt um wieviel verschoben gezeichnet wird
	jngl::Vec2 getOffset() const;

	ButtonType type;

	enum class Part {
		Left,
		Right,
		Top,
		Bottom,
	};
	Part part = Part::Left;

	mutable std::unique_ptr<jngl::Sprite> sprite;
	mutable jngl::FrameBuffer frameBuffer;

	bool collected = false;

	/// Wird nur für die Position verwendet, wenn es keinen Box2D-Body mehr gibt
	jngl::Vec2 position;

	/// Die Position zu der wir hin animieren wollen
	jngl::Vec2 targetPosition;

	jngl::Sprite shadow{"shadow"};

	// wird ungleichmäßig hochgezählt fürs auf und ab wippen
	float time = 0;

	int lifetime = 1800;
};
