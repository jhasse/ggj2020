#include "IntroMovie.hpp"

#include "engine/SkeletonDrawable.hpp"
#include "Game.hpp"

bool IntroMovie::finished = false;

void IntroMovie::animationStateListener(spAnimationState* state, spEventType type,
                                        spTrackEntry* entry, spEvent* event) {
	finished = true;

	switch (type) {
	case SP_ANIMATION_INTERRUPT:
		break;

	default:
		break;
	}
}

IntroMovie::IntroMovie() {
	// Hier die Spine JSON laden und dann:
	//
	std::string skeletonFile = "intro/intro.json";
	spAtlas* atlas = spAtlas_createFromFile("intro/intro.atlas", 0);
	spSkeletonJson* json = spSkeletonJson_create(atlas);
	// json->scale = scale;

	skeletonData = spSkeletonJson_readSkeletonDataFile(json, (skeletonFile).c_str());
	if (!skeletonData) {
		// this->loading = RESOURCE_FAILED_LOADING;
		throw std::runtime_error("Could not parse JSON " + skeletonFile);
	}
	spSkeletonJson_dispose(json);

	spAnimationStateData* animationStateData = spAnimationStateData_create(skeletonData);

	skeleton = std::make_unique<spine::SkeletonDrawable>(skeletonData, animationStateData);
	spAnimationState_setAnimationByName(skeleton->state, 0, "animation", false);
	skeleton->state->listener = (spAnimationStateListener) & this->animationStateListener;

	skeleton->step();
}

IntroMovie::~IntroMovie() = default;

void IntroMovie::step() {
	if (!skeleton || finished) {
		jngl::setWork(std::make_shared<Game>());
		return;
	}
	skeleton->step();
}

void IntroMovie::draw() const {
	jngl::pushMatrix();
	jngl::scale(0.55);
	jngl::translate(540, 400);
	skeleton->draw();
	jngl::popMatrix();
}
