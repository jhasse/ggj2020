#include "ShootAction.hpp"

#include "Player.hpp"
#include "constants.hpp"

ShootAction::ShootAction(b2World& world, uint16 bit_mask, jngl::Vec2 position, jngl::Vec2 direction, const bool lethal)
: StunAction(world, position, lethal), direction(direction), ball("ball", true) {
	stun_time = 4;
	speed = 2.5;

	b2CircleShape shape;
	shape.m_radius = 10 / PIXEL_PER_METER;;

	CreateFixture(shape, bit_mask);
	body->GetUserData().pointer = reinterpret_cast<uintptr_t>(static_cast<GameObject*>(this));
}

bool ShootAction::step() {
	body->SetLinearVelocity(b2Vec2(direction.x * speed, direction.y * speed));
	jngl::Vec2 pos = getPosition();
	float width = jngl::getScreenWidth() / 2.f;
	float height = jngl::getScreenHeight() / 2.f;
	ball.step();

	// ueber den Bildschirmrand
	if (pos.x < -width || pos.x > width || pos.y > height || pos.y < -height) {
		return true;
	}
	return destroy;
}

void ShootAction::draw() const {
	StunAction::draw();

	jngl::pushMatrix();
	jngl::translate(getPosition());
	ball.draw();
	jngl::translate(jngl::Vec2(0, 10));
	shadow.draw();
	jngl::popMatrix();
}

void ShootAction::onContact(GameObject* other) {
	if (const auto player = dynamic_cast<Player*>(other)) {
		destroy = true;
	}
}

void ShootAction::drawReflection() const {
	StunAction::draw();

	jngl::pushMatrix();
	jngl::translate(getPosition() + jngl::Vec2(0, 19));
	jngl::scale(1, -1);
	ball.draw();
	jngl::popMatrix();
}

