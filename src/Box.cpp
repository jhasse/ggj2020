#include "Box.hpp"
#include "PunchAction.hpp"
#include "constants.hpp"

Box::Box(b2World& world, const jngl::Vec2 position) {
	b2BodyDef bodyDef;
	bodyDef.type = b2_dynamicBody;
	bodyDef.position = pixelToMeter(position);
	body = world.CreateBody(&bodyDef);
	body->SetLinearDamping(10.f);
	body->SetAngularDamping(10);

	body->GetUserData().pointer = reinterpret_cast<uintptr_t>(static_cast<GameObject*>(this));

	b2PolygonShape shape;
	shape.SetAsBox(32. / PIXEL_PER_METER / 2., 32. / PIXEL_PER_METER / 2.);
	createFixtureFromShape(shape);
}

bool Box::step() {
	checkOutOfScreen();
	return false;
}

void Box::draw() const {
	jngl::pushMatrix();
	const auto transform = body->GetTransform();
	jngl::translate(meterToPixel(transform.p));
	jngl::rotate(transform.q.GetAngle() * 180 / M_PI);
	sprite.draw();
	jngl::popMatrix();
}

void Box::onContact(GameObject* other){
	if(const auto punch = dynamic_cast<PunchAction*>(other)) {
		b2Vec2 vec = body->GetPosition() - pixelToMeter(punch->getPosition());
		body->ApplyLinearImpulse(vec, body->GetPosition(), true);
	}
}