#include "Paths.hpp"

#include <sstream>

#if defined(__linux__)
	#include <experimental/filesystem>
	namespace fs = std::experimental::filesystem;
#else
	#include <filesystem>
	namespace fs = std::filesystem;
#endif
#include <jngl.hpp>

Paths::Paths() {
#if defined(__linux__) || defined(__APPLE__)
	if (fs::exists("../../data")) { // Using Xcode?
		fs::current_path("../../data");
	} else {
		fs::current_path(fs::path(jngl::getBinaryPath()) / fs::path("../data"));
	}
#else
	auto dataFolder = fs::path(jngl::getBinaryPath()) / fs::path("../data");
	if (!fs::exists(dataFolder)) {
		dataFolder = fs::path(jngl::getBinaryPath()) / fs::path("../../data");
	}
	fs::current_path(dataFolder);
#endif
}

std::string Paths::Graphics() {
	return graphics_;
}

std::string Paths::fonts() const {
	return "fonts/";
}

void Paths::setGraphics(const std::string& graphics) {
	graphics_ = graphics;
}

Paths& getPaths() {
	return *Paths::handle();
}

std::string Paths::OriginalGfx() const {
	return originalGfx_;
}

void Paths::setOriginalGfx(const std::string& originalGfx) {
	originalGfx_ = originalGfx;
}
