#include "StunAction.hpp"

#include "DebugDraw.hpp"

StunAction::StunAction(b2World& world, jngl::Vec2 position, const bool lethal) : lethal(lethal) {
	b2BodyDef bodyDef;
	bodyDef.type = b2_dynamicBody;
	bodyDef.position = pixelToMeter(position);
	body = world.CreateBody(&bodyDef);
}

StunAction::~StunAction() {
	body->GetWorld()->DestroyBody(body);
}

void StunAction::CreateFixture(b2Shape& shape, uint16 bit_mask) {
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = 1.0f;
	fixtureDef.friction = 0.7f;
	fixtureDef.restitution = 0.1f;
	fixtureDef.filter.categoryBits = FILTER_CATEGORY_SOLID_OBJECT;
	fixtureDef.filter.maskBits = 0xffff & ~bit_mask;
	body->CreateFixture(&fixtureDef);
}

int StunAction::getStunTime() {
	return stun_time * jngl::getStepsPerSecond();
}

bool StunAction::step() {
	return false;
}

void StunAction::draw() const {
	DrawShape(body);
}

bool StunAction::isLethal() const {
	return lethal;
}
