#include "Button.hpp"

#include "ButtonFragment.hpp"

Button::Button(const ButtonType type) : type(type) {
}

void Button::step() {
	for (const auto& fragment : fragments) {
		fragment->step();
	}
	if(type == ButtonType::A || type == ButtonType::B){
		cooldown -= 0.008f;
	}
	else if(type == ButtonType::X || type == ButtonType::Y){
		cooldown -= 0.004f;
	}
	if (blink > 0) {
		--blink;
	}
}

void Button::draw() const {
	if (blink % 10 > 5) {
		return;
	}
	for (const auto& fragment : fragments) {
		fragment->draw();
	}
	if (isRepaired() && cooldown > 0) {
		jngl::setColor(0, 0, 0, 180);
		const auto pos = (fragments[0]->getTargetPosition() + fragments[1]->getTargetPosition() +
		                  fragments[2]->getTargetPosition() + fragments[3]->getTargetPosition()) /
		                 4.;
		jngl::drawEllipse(pos + jngl::Vec2(1.5, 1.5), 13, 13, (1.f - cooldown) * 2 * M_PI);
	}
}

void Button::addFragment(std::shared_ptr<ButtonFragment> fragment, const jngl::Vec2 position) {
	fragment->setTargetPosition(position + getOffset());
	fragment->collect();
	fragments.emplace_back(std::move(fragment));
}

bool Button::isRepaired() const {
	assert(fragments.size() <= 4);
	return fragments.size() == 4;
}

bool Button::use() {
	if (!isRepaired()) {
		blink = 100;
		return false;
	}
	if (cooldown < 0) {
		cooldown = 1;
		return true;
	}
	return false;
}

bool Button::isMissing(const ButtonFragment& other) {
	if (type == other.getType()) {
		for (const auto& fragment : fragments) {
			if (fragment->isSamePart(other)) {
				return false;
			}
		}
		return true;
	}
	return false;
}

jngl::Vec2 Button::getOffset() const {
	jngl::Vec2 offsetFromCenter;
	switch (type) {
	case ButtonType::A:
	case ButtonType::Down:
		offsetFromCenter = { 0, 19 };
		break;
	case ButtonType::B:
	case ButtonType::Right:
		offsetFromCenter = { 19, 0 };
		break;
	case ButtonType::X:
	case ButtonType::Left:
		offsetFromCenter = { -19, 0 };
		break;
	case ButtonType::Y:
	case ButtonType::Up:
		offsetFromCenter = { 0, -19 };
		break;
	default:
		return {};
	}
	switch (type) {
	case ButtonType::A:
	case ButtonType::B:
	case ButtonType::X:
	case ButtonType::Y:
		return offsetFromCenter + jngl::Vec2(32, 0);
	case ButtonType::Down:
	case ButtonType::Right:
	case ButtonType::Left:
	case ButtonType::Up:
		return offsetFromCenter - jngl::Vec2(32, 0);
	default:
		return {};
	}
}
