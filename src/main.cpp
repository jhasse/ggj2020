#include "constants.hpp"
#include "Intro.hpp"

#include <ctime>
#include <jngl/main.hpp>
#include <jngl/message.hpp>
#include <jngl/input.hpp>
#include <jngl/job.hpp>
#include <jngl/init.hpp>

class QuitWithEscape : public jngl::Job {
public:
	void step() override {
		if (jngl::keyPressed(jngl::key::Escape)) {
			jngl::quit();
		}
	}
	void draw() const override {
	}
};

jngl::AppParameters jnglInit() {
	jngl::AppParameters params;
	std::srand(std::time(0));
	params.displayName = programDisplayName;
	params.pixelArt = true;
	params.screenSize = { 640, 360 };
	params.start = []() {
		jngl::addJob(std::make_shared<QuitWithEscape>());
		return std::make_shared<Intro>();
	};
	return params;
}
