#include "PunchAction.hpp"
#include "constants.hpp"

PunchAction::PunchAction(b2World& world, uint16 bit_mask, jngl::Vec2 position, const bool lethal) : StunAction(world, position, lethal) {
	stun_time = 7;

	b2CircleShape shape = b2CircleShape();
	shape.m_radius = 20 / PIXEL_PER_METER;

	CreateFixture(shape, bit_mask);
	body->GetUserData().pointer = reinterpret_cast<uintptr_t>(static_cast<GameObject*>(this));
}
