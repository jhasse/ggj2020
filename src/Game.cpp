#include "Game.hpp"

#include "Box.hpp"
#include "constants.hpp"
#include "ButtonFragment.hpp"
#include "Player.hpp"
#include "WinningScreen.hpp"

#include <cmath>
#include <map>

Game::Game()
: world({ 0, 0 /* gravity */ }), groundPosition(pixelToMeter({ 0, GROUND + PIXEL_PER_METER })) {

	reflectionFb = std::make_unique<jngl::FrameBuffer>(jngl::getWindowSize());
	{
		fragmentShader = std::make_unique<jngl::Shader>(jngl::readAsset("blur.frag").str().c_str(),
		                                                jngl::Shader::Type::FRAGMENT);
	}
	shaderProgram =
	    std::make_unique<jngl::ShaderProgram>(jngl::Sprite::vertexShader(), *fragmentShader);

	world.SetContactListener(&contactListener);

	b2BodyDef bodyDef;
	bodyDef.position = groundPosition;
	bodyDef.type = b2_kinematicBody;
	ground = world.CreateBody(&bodyDef);

	b2PolygonShape shape;
	shape.SetAsBox(BOUNDS_W / PIXEL_PER_METER, 1);
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = 1.0f;
	fixtureDef.friction = 0.7f;
	fixtureDef.restitution = 0.1f;
	fixtureDef.filter.categoryBits = FILTER_CATEGORY_SOLID_OBJECT;
	fixtureDef.filter.maskBits = 0xffff;
	ground->CreateFixture(&fixtureDef);

	// gameObjects.emplace_back(std::make_shared<Area>(world, jngl::Vec2(0,0)));
	gameObjects.emplace_back(std::make_shared<Player>(world, jngl::Vec2(-100, 100), 2));
	gameObjects.emplace_back(std::make_shared<Player>(world, jngl::Vec2(-100, -100), 3));
	gameObjects.emplace_back(std::make_shared<Player>(world, jngl::Vec2(100, 100), 1));
	gameObjects.emplace_back(std::make_shared<Player>(world, jngl::Vec2(100, -100), 0));

	gameObjects.emplace_back(std::make_shared<Box>(world, jngl::Vec2(-100, 0)));
	gameObjects.emplace_back(std::make_shared<Box>(world, jngl::Vec2(100, 0)));
	gameObjects.emplace_back(std::make_shared<Box>(world, jngl::Vec2(0, 100)));
	gameObjects.emplace_back(std::make_shared<Box>(world, jngl::Vec2(0, -100)));

	jngl::loop("sfx/song.ogg");
}

Game::~Game() {
	gameObjects.clear();
}

void Game::step() {
	world.Step(playerWon ? (1.f / 120.f) : (1.f / 60.f), 8, 3);

	playersAliveCount = 0;
	for (auto it = gameObjects.begin(); it != gameObjects.end(); ++it) {
		if ((*it)->step()) {
			it = gameObjects.erase(it);
			if (it == gameObjects.end()) {
				break;
			}
		} else if (const auto player = dynamic_cast<Player*>(it->get())) {
			if (player->isAlive()) {
				++playersAliveCount;
				possibleWinner = player;
			}
		}
	}
	if (!playerWon && playersAliveCount <= 1) {
		playerWon = possibleWinner;
		assert(jngl::isPlaying("sfx/final_song.ogg"));
		jngl::stop("sfx/final_song.ogg");
		jngl::play("sfx/win_song.ogg");
		jngl::setWork(std::make_shared<WinningScreen>(jngl::getWork()));
	}
	for (auto it = animations.begin(); it != animations.end(); ++it) {
		if ((*it)->step()) {
			it = animations.erase(it);
			if (it == animations.end()) {
				break;
			}
		}
	}

	for (const auto& obj : gameObjects) {
		if (const auto player = dynamic_cast<Player*>(obj.get())) {
			for (const auto& obj : gameObjects) {
				if (const auto player2 = dynamic_cast<Player*>(obj.get())) {
					player->playerContactHUD(player2->getPosition());
				}
			}
		}
	}

	while (--spawnCountdown <= 0) {
		const auto buttonType = static_cast<ButtonType>(rand() % 8);
		auto fragment = std::make_shared<ButtonFragment>(world, buttonType, rand() % 4);

		bool someoneMissesIt = false;

		int noPlayersNotRepaired = 0;
		for (const auto& obj : gameObjects) {
			if (const auto player = dynamic_cast<Player*>(obj.get())) {
				if (!player->isAlive()) {
					continue;
				}
				if (!player->isRepaired()) {
					++noPlayersNotRepaired;
				}
				if (player->isMissing(*fragment)) {
					someoneMissesIt = true;
					break;
				}
			}
		}

		if (noPlayersNotRepaired == 0) {
			break;
		}

		if (!someoneMissesIt) {
			continue; // Nur Fragments spawnen, die noch mindestens einem fehlen
		}
		spawnCountdown = 100;
		float offset = 15;
		fragment->setPosition(
		    jngl::Vec2(rand() % std::lround(jngl::getScreenSize().x - offset * 2) + offset,
		               rand() % std::lround(jngl::getScreenSize().y - offset * 2) + offset) -
		    jngl::getScreenSize() / 2.);
		gameObjects.emplace_back(std::move(fragment));
	}

	if (!playerWon) {
		for (const auto& obj : gameObjects) {
			if (const auto player = dynamic_cast<Player*>(obj.get())) {
				if (player->isRepaired() && !player->isKing()) {
					player->setKing(true);
					playerWon = player;
					if (jngl::isPlaying("sfx/song.ogg")) {
						jngl::stop("sfx/song.ogg");
						jngl::loop("sfx/final_song.ogg");
					}
					jngl::setWork(std::make_shared<WinningScreen>(jngl::getWork()));
				}
			}
		}
	}

#ifndef NDEBUG
	if (jngl::keyPressed(jngl::key::F5)) {
		jngl::setWork(std::make_shared<Game>());
	}
#endif
}

void Game::draw() const {
	floor.draw();
	std::multimap<double, GameObject*> orderedByZIndex;
	std::vector<const Player*> players;
	for (const auto& obj : gameObjects) {
		orderedByZIndex.emplace(obj->getZIndex(), obj.get());
		if (const auto player = dynamic_cast<Player*>(obj.get())) {
			players.emplace_back(player);
		}
	}

	/* Reflektionen auf dem Boden. Erstmal deaktiviert :)
	{
		auto fbContext = reflectionFb->use();
		reflectionFb->clear();
		for (const auto& gameObject : orderedByZIndex) {
			gameObject.second->drawReflection();
		}
	}
	jngl::pushSpriteAlpha(90);
	reflectionFb->draw(-jngl::getScreenSize() / 2., shaderProgram.get());
	jngl::popSpriteAlpha();
	*/

	for (const auto& gameObject : orderedByZIndex) {
		gameObject.second->draw();
	}

	for (const auto player : players) {
		if (player == playerWon) {
			const auto blink = std::lround(jngl::getTime() * 3);
			if (blink % 2 == 0) {
				player->drawHUD();
			}
		} else {
			player->drawHUD();
		}
	}

	for (auto& animation : animations) {
		animation->draw();
	}
}

int Game::playersAlive() const {
	return playersAliveCount;
}

void Game::onLoad() {
	// Wird z. B. aufgerufen, wenn wir vom WinningScreen wieder aktiv gesetzt wurden.
	playerWon = nullptr;
}
