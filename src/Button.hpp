#pragma once

#include "ButtonType.hpp"

#include <jngl/Vec2.hpp>
#include <memory>
#include <vector>

class ButtonFragment;

/// Der Button so wie er im HUD angezeigt wird, kein Stück zum Einsammeln
class Button {
public:
	Button(ButtonType);

	void step();
	void draw() const;

	void addFragment(std::shared_ptr<ButtonFragment>, jngl::Vec2 position);

	/// Alle Fragmente gefunden?
	bool isRepaired() const;

	bool isReader() const;

	/// Fehlt dieses Fragment noch? Wenn falscher Typ, gibt es immer false zurück
	bool isMissing(const ButtonFragment&);

	jngl::Vec2 getOffset() const;

	/// Startet den Cooldown. Gibt false zurück falls der Button noch nicht bereit ist
	bool use();

private:
	ButtonType type;

	std::vector<std::shared_ptr<ButtonFragment>> fragments;

	/// Wenn <= 0.f kann der Button benutzt werden.
	float cooldown = 0;

	int blink = 0;
};
