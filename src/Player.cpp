#include "Player.hpp"

#include "Button.hpp"
#include "ButtonFragment.hpp"
#include "DebugDraw.hpp"
#include "Gamepad.hpp"
#include "Keyboard.hpp"
#include "PunchAction.hpp"
#include "ShootAction.hpp"

#include <cmath>

Player::Player(b2World& world, const jngl::Vec2 position, const int playerNr)
: playerNr(playerNr), sprite("player" + std::to_string(playerNr)),
  spriteStunned("player" + std::to_string(playerNr) + "_stunn"), shadow("shadow"),
  idle("body_idle"), walk("body_walk", true), animation_shield("shield", true),
  animation_attack("attack", false), shield_active(false), crown("crown") {
	b2BodyDef bodyDef;
	bodyDef.position = pixelToMeter(position);
	bodyDef.type = b2_dynamicBody;
	body = world.CreateBody(&bodyDef);
	body->SetGravityScale(0);
	body->GetUserData().pointer = reinterpret_cast<uintptr_t>(static_cast<GameObject*>(this));
	body->SetLinearDamping(10.f);

	b2CircleShape shape = b2CircleShape();
	shape.m_radius = 12 / PIXEL_PER_METER;
	createFixtureFromShape(shape);

	const auto controllers = jngl::getConnectedControllers();
	if (controllers.size() > playerNr) {
		control = std::make_unique<Gamepad>(controllers[playerNr], playerNr);
	} else {
		control = std::make_unique<Keyboard>(playerNr);
	}

	switch (playerNr) {
	case 0:
		hudPosition = { 250, -145 };
		break;
	case 1:
		hudPosition = { 250, 145 };
		break;
	case 2:
		hudPosition = { -250, 145 };
		break;
	case 3:
		hudPosition = { -250, -145 };
		break;
	default:
		assert(false); // ungültige Spieler-Nr
	}

	shadow.setCenter(0, 10);
	crown.setCenter(0, -15);

	buttons[static_cast<int>(ButtonType::A)] = std::make_unique<Button>(ButtonType::A);
	buttons[static_cast<int>(ButtonType::B)] = std::make_unique<Button>(ButtonType::B);
	buttons[static_cast<int>(ButtonType::X)] = std::make_unique<Button>(ButtonType::X);
	buttons[static_cast<int>(ButtonType::Y)] = std::make_unique<Button>(ButtonType::Y);
	buttons[static_cast<int>(ButtonType::Left)] = std::make_unique<Button>(ButtonType::Left);
	buttons[static_cast<int>(ButtonType::Right)] = std::make_unique<Button>(ButtonType::Right);
	buttons[static_cast<int>(ButtonType::Up)] = std::make_unique<Button>(ButtonType::Up);
	buttons[static_cast<int>(ButtonType::Down)] = std::make_unique<Button>(ButtonType::Down);

	// Damit man am Anfang laufen kann, sollte man schon zwei orthogonale Richtungen bekommen:
	for (auto buttonType : { ButtonType::Left, ButtonType::Up }) {
		if (rand() % 2 == 0) {
			if (buttonType == ButtonType::Left) {
				buttonType = ButtonType::Right;
			}
			if (buttonType == ButtonType::Up) {
				buttonType = ButtonType::Down;
			}
		}
		for (int partNr = 0; partNr < 4; ++partNr) {
			const auto& button = buttons[static_cast<int>(buttonType)];
			auto fragment = std::make_shared<ButtonFragment>(world, buttonType, partNr);
			assert(button->isMissing(*fragment));
			fragment->setPosition(position);
			button->addFragment(std::move(fragment), hudPosition);
		}
	}

	int fragmentsAdded = 0;
	// Dann kriegt man noch 5 zufällige Fragmente - AUFS HAUS!
	// Hier 23 einsetzen damit nur noch ein Fragment eingesammelt werden muss.
#ifdef NDEBUG
	while (fragmentsAdded < 5) {
#else
	while (fragmentsAdded < 23) {
#endif
		const auto buttonType = static_cast<ButtonType>(rand() % 8);
		const auto& button = buttons[static_cast<int>(buttonType)];
		auto fragment = std::make_shared<ButtonFragment>(world, buttonType, rand() % 4);
		if (button->isMissing(*fragment)) {
			fragment->setPosition(position);
			button->addFragment(std::move(fragment), hudPosition);
			++fragmentsAdded;
		}
	}
}

Player::~Player() {
	if (body) {
		body->GetWorld()->DestroyBody(body);
	}
}

bool Player::step() {
	hudCollision = false;

	for (auto& button : buttons) {
		button->step();
	}

	// is player stunned? skip step
	if (stun_time > 0) {
		stun_time--;
		return false;
	}
	if (!alive) {
		return false;
	}
	// remove punch
	punchAction = NULL;

	walk.step();
	animation_shield.step();
	if (has_punched) {
		animation_attack.step();
	}
	if (animation_attack.Finished()) {
		has_punched = false;
		animation_attack.reset();
	}

	checkOutOfScreen();
	jngl::Vec2 vec = control->getMovement() * 0.8;
	if (!buttons[static_cast<int>(ButtonType::Left)]->isRepaired() && vec.x < 0) {
		vec.x = 0;
	}
	if (!buttons[static_cast<int>(ButtonType::Right)]->isRepaired() && vec.x > 0) {
		vec.x = 0;
	}
	if (!buttons[static_cast<int>(ButtonType::Up)]->isRepaired() && vec.y < 0) {
		vec.y = 0;
	}
	if (!buttons[static_cast<int>(ButtonType::Down)]->isRepaired() && vec.y > 0) {
		vec.y = 0;
	}
	if (dashCountdown > 0) {
		--dashCountdown;
		vec *= 4;
	}
	body->SetLinearVelocity(b2Vec2(vec.x, vec.y));

	time += float(90 + (rand() % 10)) / 1000.f;
	if (shield_up_time > 0) {
		shield_up_time--;
	} else {
		shield_active = false;
	}

	if (control->dash()) {
		if (buttons[static_cast<int>(ButtonType::A)]->use()) {
			Player::dash(vec);
		} else {
			vibrate();
		}
	}
	if (control->punsh()) {
		if (buttons[static_cast<int>(ButtonType::B)]->use()) {
			punsh();
		} else {
			vibrate();
		}
	}
	if (control->shoot()) {
		if (buttons[static_cast<int>(ButtonType::X)]->use()) {
			shoot();
		} else {
			vibrate();
		}
	}
	if (control->shield()) {
		if (buttons[static_cast<int>(ButtonType::Y)]->use()) {
			if (!shield_active) {
				shield();
				shield_up_time = 120;
			}
		} else {
			vibrate();
		}
	}

	for (auto it = shootActions.begin(); it != shootActions.end(); ++it) {
		if ((*it)->step()) {
			it = shootActions.erase(it);
			if (it == shootActions.end()) {
				break;
			}
		}
	}

	return false;
}

void Player::dash(const jngl::Vec2 vec) {
	dashDirection.x = vec.x * body->GetMass();
	dashDirection.y = vec.y * body->GetMass();
	dashCountdown = 15;
	jngl::play("sfx/dash.ogg");
}

void Player::punsh() {
	b2Fixture fixture = body->GetFixtureList()[0];
	uint16 bit_mask = fixture.GetFilterData().categoryBits;
	punchAction = std::make_unique<PunchAction>(*body->GetWorld(), bit_mask, getPosition(), isKing());
	has_punched = true;
	jngl::play("sfx/hit.ogg");
}

void Player::shield() {
	jngl::play("sfx/shield.ogg");
	shield_active = true;
}

void Player::shoot() {
	jngl::play("sfx/shoot.ogg");
	b2Fixture fixture = body->GetFixtureList()[0];
	uint16 bit_mask = fixture.GetFilterData().categoryBits;
	jngl::Vec2 direction = control->getMovement();
	if (direction.x == 0 && direction.y == 0) {
		// we need to move to shoot
		return;
	}
	shootActions.push_back(
	    std::make_shared<ShootAction>(*body->GetWorld(), bit_mask, getPosition(), direction, isKing()));
}

void Player::draw() const {
	jngl::pushMatrix();
	jngl::translate(getPosition());
	shadow.draw();
	if (playerNr == 0) {
		jngl::setSpriteColor(51, 255, 51);
	} else if (playerNr == 1) {
		jngl::setSpriteColor(255, 0, 51);
	} else if (playerNr == 2) {
		jngl::setSpriteColor(51, 51, 255);
	} else if (playerNr == 3) {
		jngl::setSpriteColor(255, 255, 51);
	}
	if (!alive) {
		jngl::rotate(90);
	}
	{
		jngl::pushMatrix();
		jngl::translate(0, 6);
		if (alive && body->GetLinearVelocity().LengthSquared() > 0.1) {
			walk.draw();
		} else {
			idle.draw();
		}
		if (shield_active) {
			animation_shield.draw();
		}
		if (has_punched) {
			animation_attack.draw();
		}

		jngl::popMatrix();
	}
	jngl::setSpriteColor(255, 255, 255);
	jngl::translate(-jngl::Vec2(0, 8 + (alive ? std::sin(time) : 0)));
	if (!alive || stun_time > 0) {
		spriteStunned.draw();
	} else {
		sprite.draw();
	}
	if (isKing())
	{
		crown.draw();
	}
	jngl::popMatrix();

	for (auto it = shootActions.begin(); it != shootActions.end(); ++it) {
		(*it)->draw();
	}
	// DEBUG
	DrawShape(body);
	if (punchAction != NULL) {
		punchAction->draw();
	}
}

void Player::drawReflection() const {
	jngl::pushMatrix();
	jngl::translate(getPosition() + jngl::Vec2(0, 20));
	jngl::scale(1, -1);
	if (playerNr == 0) {
		jngl::setSpriteColor(51, 255, 51);
	} else if (playerNr == 1) {
		jngl::setSpriteColor(255, 0, 51);
	} else if (playerNr == 2) {
		jngl::setSpriteColor(51, 51, 255);
	} else if (playerNr == 3) {
		jngl::setSpriteColor(255, 255, 51);
	}
	if (!alive) {
		jngl::rotate(90);
	}
	{
		jngl::pushMatrix();
		jngl::translate(0, 6);
		if (alive && body->GetLinearVelocity().LengthSquared() > 0.1) {
			walk.draw();
		} else {
			idle.draw();
		}
		if (shield_active) {
			animation_shield.draw();
		}
		if (has_punched) {
			animation_attack.draw();
		}

		jngl::popMatrix();
	}
	jngl::setSpriteColor(255, 255, 255);
	jngl::translate(-jngl::Vec2(0, 8 + (alive ? std::sin(time) : 0)));
	if (!alive || stun_time > 0) {
		spriteStunned.draw();
	} else {
		sprite.draw();
	}
	if (isKing()) {
		crown.draw();
	}
	jngl::popMatrix();

	for (auto it = shootActions.begin(); it != shootActions.end(); ++it) {
		(*it)->drawReflection();
	}
}

void Player::playerContactHUD(jngl::Vec2 pos) {
	switch (playerNr) {
	case 0:
		if (pos.x > 175 && pos.y < -100) {
			hudCollision = true;
		}
		break;
	case 1:
		if (pos.x > 175 && pos.y > 100) {
			hudCollision = true;
		}
		break;
	case 2:
		if (pos.x < -175 && pos.y > 100) {
			hudCollision = true;
		}
		break;
	case 3:
		if (pos.x < -175 && pos.y < -100) {
			hudCollision = true;
		}
		break;
	default:
		break;
	}
}

void Player::drawHUD() const {

	jngl::pushMatrix();
	jngl::translate(hudPosition);
	if (hudCollision) {
		jngl::pushSpriteAlpha(150);
	} else {
		jngl::pushSpriteAlpha(230);
	}
	gamepad.draw();
	jngl::setFontSize(10);
	jngl::setFontColor(0_rgb);
	jngl::popSpriteAlpha();
	// jngl::print("fragments: " + std::to_string(fragments.size()), {-50, 0});
	jngl::popMatrix();
	jngl::pushSpriteAlpha(hudCollision ? 200 : 255);
	for (const auto& button : buttons) {
		button->draw();
	}
	jngl::popSpriteAlpha();
}

void Player::onContact(GameObject* other) {
	if (!alive) {
		return;
	}
	if (const auto fragment = dynamic_cast<ButtonFragment*>(other)) {
		for (const auto& button : buttons) {
			if (button->isMissing(*fragment)) {
				jngl::play("sfx/grab.ogg");
				button->addFragment(fragment->shared_from_this(), hudPosition);
				break;
			}
		}
	} else if (const auto action = dynamic_cast<StunAction*>(other)) {
		if (!shield_active) {
			alive = !action->isLethal();
			if (alive) {
				stun_time = action->getStunTime();
				jngl::play("sfx/get_hit.ogg");
			} else {
				jngl::play("sfx/scream.ogg");
			}
		} else {
			jngl::play("sfx/deny.ogg");
		}
	}
}

uint16 getFilterCategory(int playerNr) {
	switch (playerNr) {
	case 0:
		return FILTER_CATEGORY_PLAYER0;
	case 1:
		return FILTER_CATEGORY_PLAYER1;
	case 2:
		return FILTER_CATEGORY_PLAYER2;
	case 3:
		return FILTER_CATEGORY_PLAYER3;
	default:
		break;
	}
	return FILTER_CATEGORY_SOLID_OBJECT;
}

bool Player::isRepaired() const {
	for (const auto& button : buttons) {
		if (!button->isRepaired()) return false;
	}
	return true;
}

bool Player::isAlive() const {
	return alive;
}

bool Player::isKing() const {
	return king;
}

void Player::setKing(bool king) {
	this->king = king;
}

void Player::vibrate() {
	control->vibrate();
}

void Player::createFixtureFromShape(const b2Shape& shape) {
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = 1.0f;
	fixtureDef.friction = 0.7f;
	fixtureDef.restitution = 0.1f;
	fixtureDef.filter.categoryBits = getFilterCategory(playerNr);
	fixtureDef.filter.maskBits = 0xffff;
	body->CreateFixture(&fixtureDef);
	body->SetGravityScale(1);
}

bool Player::isMissing(const ButtonFragment& fragment) const {
	for (const auto& button : buttons) {
		if (button->isMissing(fragment)) {
			return true;
		}
	}
	return false;
}
