#pragma once

#include "StunAction.hpp"

class PunchAction : public StunAction {
public:
	PunchAction(b2World& world, uint16 bit_mask, jngl::Vec2 position, bool lethal);
};