#include "ButtonFragment.hpp"

#include "constants.hpp"

#include <cmath>
#include <stdexcept>
#include <algorithm>

double clamp(double v, double l, double h) {
	if (v < l) { return l; }
	if (v > h) { return h; }
	return v;
}

ButtonFragment::ButtonFragment(b2World& world, const ButtonType type, const int partNr)
: type(type), frameBuffer(22_sp, 22_sp) {
	b2BodyDef bodyDef;
	bodyDef.type = b2_dynamicBody;
	body = world.CreateBody(&bodyDef);
	body->SetGravityScale(0);
	body->GetUserData().pointer = reinterpret_cast<uintptr_t>(static_cast<GameObject*>(this));

	b2CircleShape shape = b2CircleShape();
	shape.m_radius = pixelToMeter(7);
	b2FixtureDef fixtureDef;
	fixtureDef.isSensor = true;
	fixtureDef.shape = &shape;
	fixtureDef.density = 1.0f;
	fixtureDef.friction = 0.7f;
	fixtureDef.restitution = 0.1f;
	fixtureDef.filter.categoryBits = FILTER_CATEGORY_SOLID_OBJECT;
	fixtureDef.filter.maskBits = 0xffff;
	body->CreateFixture(&fixtureDef);
	body->SetGravityScale(1);

	part = static_cast<Part>(partNr);
}

ButtonFragment::~ButtonFragment() {
	if (body) {
		body->GetWorld()->DestroyBody(body);
	}
}

bool ButtonFragment::step() {
	time += float(90 + (rand() % 10)) / 1000.f;
	if (body && collected) {
		body->GetWorld()->DestroyBody(body);
		body = nullptr;
		return true;
	}
	if (!body) {
		position += (targetPosition - position) / 20.0;
	}
	return collected || --lifetime < 40;
}

void ButtonFragment::draw() const {
	if (!sprite) {
		switch (type) {
		case ButtonType::A:
			sprite = std::make_unique<jngl::Sprite>("button_a");
			break;
		case ButtonType::B:
			sprite = std::make_unique<jngl::Sprite>("button_b");
			break;
		case ButtonType::X:
			sprite = std::make_unique<jngl::Sprite>("button_x");
			break;
		case ButtonType::Y:
			sprite = std::make_unique<jngl::Sprite>("button_y");
			break;
		case ButtonType::Left:
			sprite = std::make_unique<jngl::Sprite>("direction_left");
			break;
		case ButtonType::Right:
			sprite = std::make_unique<jngl::Sprite>("direction_right");
			break;
		case ButtonType::Down:
			sprite = std::make_unique<jngl::Sprite>("direction_down");
			break;
		case ButtonType::Up:
			sprite = std::make_unique<jngl::Sprite>("direction_up");
			break;
		default:
			throw std::runtime_error("TODO");
		}
		jngl::pushMatrix();
		jngl::reset();
		const auto fbContext = frameBuffer.use();
		jngl::translate(-frameBuffer.getSize() / 2.);
		jngl::translate(sprite->getWidth() / 2., sprite->getHeight() / 2.);

		// Verschieben, damit je nach Ausrichtung das Zentrum trotzdem ungefähr in der Mitte liegt:
		jngl::translate(getOffset());

		sprite->draw();
		{
			const auto guard = jngl::disableBlending();
			jngl::setColor(0, 0, 0, 0);
			if (part != Part::Right) {
				jngl::drawTriangle({ 0, 0 }, { 99, 99 }, { 99, -99 });
			}
			if (part != Part::Left) {
				jngl::drawTriangle({ 0, 0 }, { -99, 99 }, { -99, -99 });
			}
			if (part != Part::Bottom) {
				jngl::drawTriangle({ 0, 0 }, { 99, 99 }, { -99, 99 });
			}
			if (part != Part::Top) {
				jngl::drawTriangle({ 0, 0 }, { 99, -99 }, { -99, -99 });
			}
		}
		jngl::popMatrix();
	}
	jngl::pushMatrix();
	jngl::translate(getPosition());

	if (body && lifetime < 255) {
		jngl::pushSpriteAlpha(lifetime < 0 ? 0 : lifetime);
	}
	float zoomIn = clamp(time * 0.2f, 0.f, 1.f);
	if (body) {
		shadow.draw(
		    jngl::modelview()
		        .translate({ 1, 4 }) // etwas nach unten verschieben damit es dadrunter ist
		        .scale(zoomIn + std::sin(time) * 0.03));
		jngl::translate(-jngl::Vec2(0, 8 + std::sin(time)));
	}
	frameBuffer.draw(-frameBuffer.getSize() / 2. - jngl::Vec2(0, (1 - zoomIn) * jngl::getScreenSize().y));
	if (body && lifetime < 255) {
		jngl::popSpriteAlpha();
	}
	jngl::popMatrix();
}

void ButtonFragment::drawReflection() const {
	jngl::pushMatrix();
	jngl::translate(getPosition() + jngl::Vec2(0, 9));
	jngl::scale(1, -1);

	if (body && lifetime < 255) {
		jngl::pushSpriteAlpha(lifetime < 0 ? 0 : lifetime);
	}
	float zoomIn = clamp(time * 0.2f, 0.f, 1.f);
	if (body) {
		jngl::translate(-jngl::Vec2(0, 8 + std::sin(time)));
	}
	frameBuffer.draw(-frameBuffer.getSize() / 2. -
	                 jngl::Vec2(0, (1 - zoomIn) * jngl::getScreenSize().y));
	if (body && lifetime < 255) {
		jngl::popSpriteAlpha();
	}
	jngl::popMatrix();
}

void ButtonFragment::collect() {
	position = getPosition();
	time = 999.f; // Kein Reinfallen mehr
	collected = true;
}

jngl::Vec2 ButtonFragment::getPosition() const {
	if (body) {
		return GameObject::getPosition();
	}
	return position;
}

void ButtonFragment::setTargetPosition(jngl::Vec2 targetPosition) {
	this->targetPosition = targetPosition - getOffset();
}

jngl::Vec2 ButtonFragment::getTargetPosition() const {
	return targetPosition;
}

ButtonType ButtonFragment::getType() const {
	return type;
}

bool ButtonFragment::isSamePart(const ButtonFragment &other) {
	return other.part == part;
}

double ButtonFragment::getZIndex() const {
	return GameObject::getZIndex() - 4;
}

jngl::Vec2 ButtonFragment::getOffset() const {
	const double OFFSET = 6;
	switch (part) {
	case Part::Left:
		return jngl::Vec2(OFFSET, 0);
		break;
	case Part::Right:
		return jngl::Vec2(-OFFSET, 0);
		break;
	case Part::Top:
		return jngl::Vec2(0, OFFSET);
		break;
	case Part::Bottom:
		return jngl::Vec2(0, -OFFSET);
		break;
	}
	return {};
}
