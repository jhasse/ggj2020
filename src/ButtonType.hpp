#pragma once

enum class ButtonType {
	A,
	B,
	X,
	Y,
	Left,
	Right,
	Up,
	Down,
	L,
	R,
};
