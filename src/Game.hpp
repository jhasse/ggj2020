#pragma once

#include "ContactListener.hpp"

#include <jngl.hpp>
#include <set>
#include <vector>

class Animation;
class GameObject;
class Player;

class Game : public jngl::Work {
public:
	Game();
	~Game();
	void onLoad() override;
	void step() override;
	void draw() const override;
	int playersAlive() const;

private:
	b2World world;

	jngl::Sprite floor{"floor"};

	/// Alle gebauten Blöcke
	std::vector<std::shared_ptr<GameObject>> gameObjects;

	std::vector<std::unique_ptr<Animation>> animations;

	b2Body* ground;
	const b2Vec2 groundPosition;

	ContactListener contactListener;

	// Wenn 0, dann spawnt ein neues Fragment
	int spawnCountdown = 10;

	/// Wird auf den Gewinner gesetzt, sobald dies der Fall ist
	GameObject* playerWon = nullptr;

	/// Zeigt auf einen Spieler der am Leben ist
	Player* possibleWinner = nullptr;

	int playersAliveCount = 0;

	std::unique_ptr<jngl::FrameBuffer> reflectionFb;
	mutable std::unique_ptr<jngl::Shader> vertexShader, fragmentShader;
	mutable std::unique_ptr<jngl::ShaderProgram> shaderProgram;
};
