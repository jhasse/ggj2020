import fileinput
import os


def applyConvert(dir):
    # fullCommand = 'mogrify.exe -resize ' + size + ' ' + dir + ' -gaussian-blur 0'
    fullCommand = 'convert -define webp:lossless=true ' + dir + ' ' + dir[:-4] + '.webp'
    print(fullCommand)
    # startCommandInProcess(fullCommand)
    os.system(fullCommand)


def replaceText(file):
    with fileinput.FileInput(file, inplace=True) as file:
        for line in file:
            print(line.replace('.png', '.webp'), end='')


def applyConvertToDirectory(dir):
    for file in os.listdir(dir):
        if file.endswith(".png") or file.endswith(".jpg"):
            applyConvert(dir + '/' + file)
        if file.endswith(".atlas"):
            replaceText(dir + '/' + file)


def remove_png(dir):
    for file in os.listdir(dir):
        if file.endswith(".png"):
            os.remove(dir + '/' + file)


FOLDERS = ['./data/']


def run():
    for folder in FOLDERS:
        subfolders = [f.path for f in os.scandir(folder) if f.is_dir()]
        subfolders.append(folder)
        for dir in list(subfolders):
            # Hack damit die Icons nicht angefasst werden.
            if 'icons' in dir:
                continue
            applyConvertToDirectory(dir)
            applyConvertToDirectory(dir)
            remove_png(dir)


if __name__ == "__main__":
    run()
